package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode._Libs.AutoLib;
import org.firstinspires.ftc.teamcode._Libs.SensorLib;

@Autonomous(name="Red/Gem/Side", group="Red/Gem")
public class RedLeftGemAuto extends OpMode {

    AutoLib.Sequence mSequence;             // the root of the sequence tree
    boolean bDone;                          // true when the programmed sequence is done
    DcMotor mMotors[];
    DcMotor motorGrab;                      // motors, some of which can be null: assumed order is fr, br, fl, bl
    DcMotor motorLift;
    Servo servoGem[];
    Servo servoGrabberRight[];
    Servo servoGrabberLeft[];
    ModernRoboticsI2cGyro mGyro1;           // gyro to use for heading information
    SensorLib.CorrectedMRGyro mCorrGyro;    // gyro corrector object
    ColorSensor mColorSensor;
    boolean bSetup, servoSetup = true;                         // true when we're in "setup mode" where joysticks tweak parameters
    SensorLib.PID mPid, mPid2;                     // PID controller for the sequence

    // parameters of the PID controller for this sequence
    float Kp = 0.1f;        // motor power proportional term correction per degree of deviation
    float Ki = 0.0f;         // ... integrator term
    float Kd = 0;             // ... derivative term
    float KiCutoff = 30.0f;    // maximum angle error for which we update integrator

    @Override
    public void init() {
        bSetup = false;      // start out in Kp/Ki setup mode
        AutoLib.HardwareFactory mf = null;
        final boolean debug = false;
        if (debug);
            //mf = new AutoLib.TestHardwareFactory(this);
        else
            mf = new AutoLib.RealHardwareFactory(this);

        // get the motors: depending on the factory we created above, these may be
        // either dummy motors that just log data or real ones that drive the hardware
        // assumed order is fr, br, fl, bl

        mMotors = new DcMotor[4];
        mMotors[0] = hardwareMap.dcMotor.get("fr");
        mMotors[1] = hardwareMap.dcMotor.get("br");
        mMotors[2] = hardwareMap.dcMotor.get("fl");
        mMotors[3] = hardwareMap.dcMotor.get("bl");

        motorGrab = hardwareMap.dcMotor.get("grab");
        motorLift = hardwareMap.dcMotor.get("lift");

        servoGem = new Servo[2];
        servoGem[0] = hardwareMap.servo.get("gem1");
        servoGem[1] = hardwareMap.servo.get("gem2");

        mMotors[2].setDirection(DcMotor.Direction.REVERSE);
        mMotors[3].setDirection(DcMotor.Direction.REVERSE);

        servoGrabberRight = new Servo[2];
        servoGrabberLeft = new Servo[2];
        servoGrabberLeft[0] = hardwareMap.servo.get("l1");
        servoGrabberLeft[1] = hardwareMap.servo.get("l2");
        servoGrabberRight[0] = hardwareMap.servo.get("r1");
        servoGrabberRight[1] = hardwareMap.servo.get("r2");

        mColorSensor = hardwareMap.colorSensor.get("color");

        /*mMotors[0].setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        mMotors[1].setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        mMotors[2].setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        mMotors[3].setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);*/

        // get hardware gyro(s) -- we support either one or two, where the second is assumed
        // to be mounted in opposite z-orientation to the first.
        mGyro1 = (ModernRoboticsI2cGyro) hardwareMap.gyroSensor.get("gyro");

        // wrap gyro(s) in an object that calibrates them and corrects their output
        mCorrGyro = new SensorLib.CorrectedMRGyro(mGyro1);
        mCorrGyro.calibrate();

        // create a PID controller for the sequence
        mPid = new SensorLib.PID(0.01f, 0.005f, 0, KiCutoff);    // make the object that implements PID control algorithm
        mPid2 = new SensorLib.PID(0.2f, 0, 0, KiCutoff);

        // create an autonomous sequence with the steps to drive
        // several legs of a polygonal course ---
        float power = 0.5f;

        // create the root Sequence for this autonomous OpMode
        mSequence = new AutoLib.LinearSequence();

        boolean bUseEncoders = false;
        if (bUseEncoders) {
            // add a bunch of encoder-counted "legs" to the sequence - use Gyro heading convention of positive degrees CW from initial heading
            // wrap motors to simplify use of encoders

            //DELETED
        }
        else {
            // add a bunch of timed "legs" to the sequence - use Gyro heading convention of positive degrees CW from initial heading
            float leg = 2;  // time along each leg of the polygon

            /*mSequence.add(new AutoLib.ServoSetPosition(servoGrabberLeft[0], 0));
            mSequence.add(new AutoLib.ServoSetPosition(servoGrabberLeft[1], 0));
            mSequence.add(new AutoLib.ServoSetPosition(servoGrabberRight[0], 1));
            mSequence.add(new AutoLib.ServoSetPosition(servoGrabberRight[1], 1));
            mSequence.add(new AutoLib.TimedMotorStep(motorLift, 0.5, 0.1, true));*/
            mSequence.add(new AutoLib.ServoSetPosition(servoGem[1], 0.0));
            mSequence.add(new AutoLib.LogTimeStep(this, "wait", 1.0));
            mSequence.add(new AutoLib.ServoSetPosition(servoGem[0], 0.95));
            mSequence.add(new AutoLib.LogTimeStep(this, "wait", 1.0));
            mSequence.add(new AutoLib.JewelStep(this, 1, mColorSensor, mMotors, power, mCorrGyro, mPid, servoGem[0]));
            mSequence.add(new AutoLib.ServoSetPosition(servoGem[1], 0.6467503));
            mSequence.add(new AutoLib.ServoSetPosition(servoGem[0], 0.0));
            mSequence.add(new AutoLib.GyroRotateStep(this, mMotors, 0.75f, mCorrGyro, mPid, 0, 3));
            /*mSequence.add(new AutoLib.LogTimeStep(this, "wait", 0.5));
            mSequence.add(new AutoLib.MoveSquirrelyByTimeStep(mMotors, 0.00, -1, 1.0f, true));
            mSequence.add(new AutoLib.SquirrelyGyroTimedDriveStep(this, 90, 0, mCorrGyro, mPid, mMotors, 1, 0.2f, true));*/

        }

        // start out not-done
        bDone = false;
    }

    @Override
    public void loop() {
        if (gamepad1.y)
            bSetup = true;      // enter "setup mode" using controller inputs to set Kp and Ki
        if (gamepad1.x)
            bSetup = false;     // exit "setup mode"

        if (bSetup) {           // "setup mode"
            // adjust PID parameters by joystick inputs
            Kp -= (gamepad1.left_stick_y * 0.0001f);
            Ki -= (gamepad1.right_stick_y * 0.0001f);
            // update the parameters of the PID used by all Steps in this test
            mPid.setK(Kp, Ki, Kd, KiCutoff);
            // log updated values to the operator's console
            telemetry.addData("Kp = ", Kp);
            telemetry.addData("Ki = ", Ki);
            return;
        }

        // until we're done, keep looping through the current Step(s)
        if (!bDone)
            bDone = mSequence.loop();       // returns true when we're done
        else
            telemetry.addData("sequence finished", "");
    }

    @Override
    public void stop() {
        super.stop();
        mCorrGyro.stop();        // release the physical sensor(s) we've been using for azimuth data
    }
}