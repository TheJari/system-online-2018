package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import org.firstinspires.ftc.teamcode._Libs.AutoLib;
import org.firstinspires.ftc.teamcode._Libs.HeadingSensor;
import org.firstinspires.ftc.teamcode._Libs.SensorLib;

import java.util.ArrayList;

/**
 * Created by jaren on 11/10/2017.
 */

public class DriverLib {
    static public class SquirrelyGyroDrive {
        private float mPower;                               // basic power setting of all 4 motors -- adjusted for steering along path
        private OpMode mOpMode;                             // needed so we can log output (may be null)
        private HeadingSensor mGyro;                        // sensor to use for heading information (e.g. Gyro or Vuforia)
        private SensorLib.PID mPid;                         // proportional–integral–derivative controller (PID controller)
        private double mPrevTime;                           // time of previous loop() call
        private DcMotor mMotors[];    // the motor steps we're guiding - assumed order is fr, br, fl, bl

        public SquirrelyGyroDrive(OpMode mode, HeadingSensor gyro, SensorLib.PID pid,
                                      DcMotor motors[]) {
            mOpMode = mode;
            mGyro = gyro;
            if (mPid != null)
                mPid = pid;     // client is supplying PID controller for correcting heading errors
            else {
                // construct a default PID controller for correcting heading errors
                final float Kp = 0.05f;        // degree heading proportional term correction per degree of deviation
                final float Ki = 0.02f;        // ... integrator term
                final float Kd = 0.0f;         // ... derivative term
                final float KiCutoff = 3.0f;   // maximum angle error for which we update integrator
                mPid = new SensorLib.PID(Kp, Ki, Kd, KiCutoff);
            }
            mMotors = motors;
        }

        public void go(float mHeading, float mDirection) {
            final float heading = mGyro.getHeading();     // get latest reading from direction sensor
            // convention is positive angles CCW, wrapping from 359-0

            final float error = SensorLib.Utils.wrapAngle(heading - mHeading);   // deviation from desired heading
            // deviations to left are positive, to right are negative

            // compute delta time since last call -- used for integration time of PID step
            final double time = mOpMode.getRuntime();
            final double dt = time - mPrevTime;
            mPrevTime = time;

            // feed error through PID to get motor power value for heading correction
            float hdCorr = mPid.loop(error, (float) dt);

            //calculate relative front/back motor powers for fancy wheels to move in requested direction
            AutoLib.MotorPowers mp = AutoLib.GetSquirrelyWheelMotorPowers(mDirection);

            //set the powers
            mMotors[0].setPower((mp.Front()-hdCorr) * mPower);  // fr
            mMotors[1].setPower((mp.Back()-hdCorr) * mPower);   // br
            mMotors[2].setPower((mp.Front()+hdCorr) * mPower);  // fl
            mMotors[3].setPower((mp.Back()+hdCorr) * mPower);   // bl

            // log some data
            if (mOpMode != null) {
                mOpMode.telemetry.addData("heading ", heading);
                mOpMode.telemetry.addData("front power ", mp.Front());
                mOpMode.telemetry.addData("back power ", mp.Back());
            }

            // guidance step always returns "done" so the CS in which it is embedded completes when
            // all the motors it's controlling are done
        }

    }

    static public float lookupAngle(float x, float y) {
        return x;
    }
}
