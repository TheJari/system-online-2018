package org.firstinspires.ftc.teamcode;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;
import android.widget.ImageView;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.internal.android.dex.util.ExceptionWithContext;
import org.firstinspires.ftc.robotcore.internal.opmode.OpModeManagerImpl;
import org.firstinspires.ftc.teamcode._Libs.AutoLib;
import org.firstinspires.ftc.teamcode._Libs.CameraLib;
import org.firstinspires.ftc.teamcode._Libs.HeadingSensor;
import org.firstinspires.ftc.teamcode._Libs.RS_Posterize;
import org.firstinspires.ftc.teamcode._Libs.SensorLib;
import org.firstinspires.ftc.teamcode._Libs.VuforiaLib_FTC2017;

import com.qualcomm.robotcore.hardware.UltrasonicSensor;

import java.util.ArrayList;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.firstinspires.ftc.teamcode._Libs.AutoLib.normalize;

interface SetMarkStep {
    public void setVuMarkString(String s);
}

class VuforiaGetMarkStep extends AutoLib.Step {

    VuforiaLib_FTC2017 mVLib;
    OpMode mOpMode;
    SetMarkStep mMTCBStep;
    AutoLib.Timer mTimer;

    public VuforiaGetMarkStep(OpMode opMode, VuforiaLib_FTC2017 VLib, SetMarkStep step) {
        mOpMode = opMode;
        mVLib = VLib;
        mMTCBStep = step;
        mTimer = new AutoLib.Timer(1.0);
    }

    public boolean loop() {
        super.loop();

        mVLib.loop();       // update recognition info
        RelicRecoveryVuMark vuMark = mVLib.getVuMark();
        boolean found = (vuMark != RelicRecoveryVuMark.UNKNOWN);
        if (found) {
            // Found an instance of the template -- tell "MoveTo.. step which one
            mMTCBStep.setVuMarkString(vuMark.toString());
        }

        // start the Timer on our first call
        if (firstLoopCall()) {
            mTimer.start();
        }

        if (mTimer.done()) {
            return true;
        }

        return found;       // done?
    }
}

class BlueFilter implements CameraLib.Filter {
    public int map(int hue) {
        // map 4 (cyan) to 5 (blue)
        if (hue == 4)
            return 5;
        else
            return hue;
    }
}

// simple data class containing info about image of one column of cryptobox
class ColumnHit {
    int mStart;
    int mEnd;
    public ColumnHit(int start, int end) {
        mStart = start;  mEnd = end;
    }
    public int start() { return mStart; }
    public int end() { return mEnd; }
    public int mid() { return (mStart+mEnd)/2; }
    public int width() { return mEnd - mStart + 1; }
}

interface SetPosterizer {
    public void setPosterizer(RS_Posterize posterizer);
}

class DiscretizePosterizer implements CameraLib.Filter {
    public int map(int pix) {
        // return an enumerated color based on the RGB color of the pixel
        int c = CameraLib.colors.eWhite.ordinal();
        if (pix == Color.RED) c = CameraLib.colors.eRed.ordinal();
        else if (pix == Color.GREEN) c = CameraLib.colors.eGreen.ordinal();
        else if (pix == Color.BLUE) c = CameraLib.colors.eBlue.ordinal();
        else if (pix == Color.CYAN) c = CameraLib.colors.eCyan.ordinal();
        else if (pix == Color.YELLOW) c = CameraLib.colors.eYellow.ordinal();
        else if (pix == Color.MAGENTA) c = CameraLib.colors.eMagenta.ordinal();
        return c;
    }
}

class GoToCryptoBoxGuideStep extends AutoLib.MotorGuideStep implements SetMarkStep, SetPosterizer {

    VuforiaLib_FTC2017 mVLib;
    String mVuMarkString;
    OpMode mOpMode;
    int mCBColumn;                      // which Cryptobox column we're looking for
    Pattern mPattern;                   // compiled regexp pattern we'll use to find the pattern we're looking for
    int mColumnOffset;                  // number of columns that have left the left-edge of the frame

    AutoLib.SquirrelyGyroGuideStep mGuideStep; // use a guide step to control motors

    CameraLib.Filter mBlueFilter;       // filter to map cyan to blue

    ArrayList<ColumnHit> mPrevColumns;  // detected columns on previous pass

    SensorLib.PID mPid;                 // proportional–integral–derivative controller (PID controller)
    ArrayList<AutoLib.SetPower> mMotorSteps;   // the motor steps we're guiding - assumed order is right ... left ...
    float mPower;                      // base power setting for motors
    float mHeading;

    float mLastBinWidth = 0;
    float mDoneCount = 0;
    boolean mDone = false;
    boolean mLookForBlue;
    boolean mRightToLeft = false;
    boolean mGoStraight;

    AutoLib.Timer mTimer;

    RS_Posterize mRsPosterizer;         // RenderScript process used to posterize images

    ImageView mView;

    Paint mPaintRed, mPaintGreen;                       // used to draw info overlays on image

    protected Bitmap bm;
    private static final Object bmLock = new Object(); //synchronization lock so we don't display and write

    Bitmap mBmOut;                      // processed bitmap we post to RC phone screen

    public GoToCryptoBoxGuideStep(OpMode opMode, VuforiaLib_FTC2017 VLib, String pattern, float power, float heading, HeadingSensor gyro, HardwareMap hardwareMap, boolean lookForBlue, boolean goStraight) {
        mOpMode = opMode;
        mCBColumn = 1;     // if we never get a cryptobox directive from Vuforia, go for the center bin
        mPattern = Pattern.compile(pattern);    // look for the given pattern of column colors
        mBlueFilter = new BlueFilter();
        mVLib = VLib;
        mMotorSteps = null;     // this will be filled in by call from parent step
        mPower = power;
        mPrevColumns = null;
        mColumnOffset = 0;
        mHeading = heading;
        mLookForBlue = lookForBlue;
        mGoStraight = goStraight;

        // construct a default PID controller for correcting heading errors
        final float Kp = 0.01f;         // degree heading proportional term correction per degree of deviation
        final float Ki = 0.0f;         // ... integrator term
        final float Kd = 0.0f;         // ... derivative term
        final float KiCutoff = 3.0f;   // maximum angle error for which we update integrator
        mPid = new SensorLib.PID(Kp, Ki, Kd, KiCutoff);

        mGuideStep = new AutoLib.SquirrelyGyroGuideStep(opMode, heading, heading, gyro, mPid, null, power);

        mTimer = new AutoLib.Timer(11.0);

        mView = (ImageView)((Activity)hardwareMap.appContext).findViewById(com.qualcomm.ftcrobotcontroller.R.id.OpenCVOverlay);
        mView.post(new Runnable() {
            @Override
            public void run() {
                mView.setAlpha(1.0f);
                mView.setVisibility(View.VISIBLE);
            }
        });

        // create stuff we'll use to draw debug info overlays on the image
        mPaintRed = new Paint();
        mPaintRed.setColor(Color.RED);
        mPaintGreen = new Paint();
        mPaintGreen.setColor(Color.GREEN);
    }

    public void setVuMarkString(String s) {
        mVuMarkString = s;

        // compute index of column that forms the left side of the desired bin.
        // this assumes the camera is mounted to the left of the carried block.
        if (mVuMarkString == "LEFT")
            mCBColumn = 0;
        else
        if (mVuMarkString == "CENTER")
            mCBColumn = 1;
        else
        if (mVuMarkString == "RIGHT")
            mCBColumn = 2;

        if(mRightToLeft)
            mCBColumn = 2 - mCBColumn;

        // if the camera is on the right side of the block, we want the right edge of the bin.
        final boolean bCameraOnRight = false;
        if (bCameraOnRight != mRightToLeft)
            mCBColumn++;
    }

    public void setPosterizer(RS_Posterize posterizer) {
        mRsPosterizer = posterizer;
    }

    public void set(ArrayList<AutoLib.SetPower> motorSteps){
        mMotorSteps = motorSteps;
        mGuideStep.set(mMotorSteps);
    }

    void finish() {
        mDone = true;
    }

    public boolean loop() {
        super.loop();

        final int minDoneCount = 5;      // require "done" test to succeed this many consecutive times
        int doneCount = 0;

        // start the Timer on our first call
        if (firstLoopCall())
            mTimer.start();

        mOpMode.telemetry.addData("Step Timer: %f", (float) mTimer.remaining());

        //mDone = mTimer.done();

        mOpMode.telemetry.addData("VuMark", "%s found", mVuMarkString);

        // get most recent frame from camera (through Vuforia)
        //RectF rect = new RectF(0,0.25f,1f,0.75f);      // middle half of the image should be enough
        //Bitmap bitmap = mVLib.getBitmap(rect, 4);                      // get cropped, downsampled image from Vuforia

        CameraLib.CameraImage frame;

        RectF rect = new RectF(0, 0, 1f, 0.67f);
        bm = mVLib.getBitmap(rect, 8);                      // get uncropped, downsampled image from Vuforia

        if (bm != null) {

            mOpMode.telemetry.addData("image size", "%d x %d", bm.getWidth(), bm.getHeight());

            // create the output bitmap we'll process and display on the RC phone screen
            mBmOut = Bitmap.createBitmap(bm.getWidth(), bm.getHeight(), Bitmap.Config.RGB_565);

            // posterize the input bitmap in RenderScript to generate the image we'll analyze and display
            mRsPosterizer.runScript(bm, mBmOut);

            // wrap the bitmap in a CameraImage so we can scan it for patterns
            frame = new CameraLib.CameraImage(mBmOut);

            // look for cryptobox columns
            // get unfiltered view of colors (hues) by full-image-height column bands
            final int bandSize = 2;         // 1 has better resolution but tends to create multiple hits on one column
            final float minFrac = 0.25f;     // minimum fraction of pixels in band that must be same color to mark it as a color
            String colString = frame.columnRep(bandSize, new DiscretizePosterizer(), null, minFrac);

            if(mRightToLeft) {
                colString = new StringBuilder(colString).reverse().toString();
            }

            // log debug info ...
            mOpMode.telemetry.addData("hue columns", colString);

            // look for occurrences of given pattern of column colors
            ArrayList<ColumnHit> columns = new ArrayList<ColumnHit>(8);       // array of column start/end indices

            for (int i=0; i<colString.length(); i++) {
                // starting at position (i), look for the given pattern in the encoded (rgbcymw) scanline
                Matcher m = mPattern.matcher(colString.substring(i));
                if (m.lookingAt()) {
                    // add start/end info about this hit to the array
                    //if(i > 1) { // the first column should not be at the very beginning
                        columns.add(new ColumnHit(i + m.start(), i + m.end() - 1));
                    //}

                    // skip over this match
                    i += m.end();
                }
            }

            // report the matches in telemetry
            for (ColumnHit h : columns) {
                mOpMode.telemetry.addData("found ", "%s from %d to %d", mPattern.pattern(), h.start(), h.end());
            }

            // add annotations to the bitmap showing detected column centers
            Canvas canvas = new Canvas(mBmOut);
            for (org.firstinspires.ftc.teamcode.ColumnHit h : columns) {
                canvas.drawLine(h.mid()*bandSize, 0, h.mid()*bandSize, mBmOut.getHeight()-1, mPaintGreen);
                canvas.drawLine(h.start()*bandSize, mBmOut.getHeight()-1, h.end()*bandSize, mBmOut.getHeight()-1, mPaintGreen);
            }

            int nCol = columns.size();

            // compute average distance between columns = distance between outermost / #bins
            float avgBinWidth = nCol>1 ? (float)(columns.get(nCol-1).end() - columns.get(0).start()) / (float)(nCol-1) : mLastBinWidth;
            mLastBinWidth = avgBinWidth;
            if(nCol == 1) { // if there is only one visible column, we estimate the bin width by the width of the column
                avgBinWidth = columns.get(0).width() * 5;
            }

            // try to handle case where a column has left (or entered) the left-edge of the frame between the prev view and this one
            if (mPrevColumns != null  &&  mPrevColumns.size()>0  &&  nCol>0  && avgBinWidth > 0) {

                // if the left-most column of the previous frame started at the left edge of the frame
                // and the left edge of the current left-most column is about a bin-width to the right of the right edge of the
                // left-most column of the previous frame
                // then it's probably the case that the current left-most column is actually the second column of the previous frame.
                if (mPrevColumns.get(0).start() == 0 && columns.get(0).start() > (mPrevColumns.get(0).end()+0.5*avgBinWidth)) {
                    mColumnOffset++;
                }

                // if the left-most column of the previous frame was not near the left edge of the frame
                // but now there is a column at the left edge, then one probably entered the frame.
                if (mColumnOffset > 0 && mPrevColumns.get(0).start() > 0.5*avgBinWidth  &&  columns.get(0).start() == 0) {
                    mColumnOffset--;
                }
            }

            mOpMode.telemetry.addData("data", "avgWidth= %f  mColOff=%d", avgBinWidth, mColumnOffset);

            boolean offToBeginning = mCBColumn < mColumnOffset;
            boolean offToEnd = nCol <= mCBColumn-mColumnOffset;
            // if we found some columns, try to correct course using their positions in the image
            if (!offToBeginning && !offToEnd) {
                // to start, we need to see all four columns to know where we're going ...
                // after that, we try to match up the columns visible in this view with those from the previous pass
                // TBD

                // compute camera offset from near-side column of target bin (whichever side camera is to the block holder)
                float cameraOffset = -0.5f;        // e.g. camera is 0.2 x bin width to the right of block centerline
                if(mRightToLeft)
                    cameraOffset = 0.5f;
                float cameraBinOffset = avgBinWidth * cameraOffset;
                // camera target is center of target column + camera offset in image-string space
                float cameraTarget = columns.get(mCBColumn-mColumnOffset).end() + cameraBinOffset;

                // show target point and camera center on image
                {
                    canvas.drawLine(bm.getWidth()/2-5, 15, bm.getWidth()/2+5, 15, mPaintGreen);
                    float x = cameraTarget*bandSize;
                    if (x>=0 && x<bm.getWidth())
                        canvas.drawLine(x, 10, x, 20, mPaintGreen);
                }

                // the above computed target point should be in the middle of the image if we're on course -
                // if not, correct our course to center it --
                // compute fractional error = fraction of image offset of target from center = [-1 .. +1]
                float error = (cameraTarget - (float)colString.length()/2.0f) / ((float)colString.length()/2.0f);

                // compute motor correction from error through PID --
                // for now, convert image-string error to angle and use standard "gyro" PID
                final float cameraHalfFOVdeg = 250f;       // half angle FOV is about 28 degrees
                float angError = error * cameraHalfFOVdeg;
                if(angError > 90) {
                    angError = 90;
                }
                if(angError < -90) {
                    angError = -90;
                }
                float power = mPower;
                if(avgBinWidth < colString.length()*.30 && avgBinWidth > 0 && mGoStraight){
                    angError = 0;
                    power = mPower - 0.1f;
                }

                if(mRightToLeft)
                    angError = -angError;

                mGuideStep.set(mHeading - angError, mHeading, power); // give guide step correct direction

                mOpMode.telemetry.addData("data", "target=%f  error=%f angError=%f doneCount=%f", cameraTarget, error, angError, mDoneCount);

                mGuideStep.loop(); // run the guide step
            }
            else {
                float angError = mRightToLeft ? 90.0f : -90.0f;

                if(offToEnd)
                    angError = -angError;

                float power = mPower;
                if(avgBinWidth < colString.length()*.30 && avgBinWidth > 0){
                    angError = 0;
                    power = mPower - 0.1f;
                }

                mGuideStep.set(mHeading - angError, mHeading, power); // give guide step correct direction

                mGuideStep.loop(); // run the guide step
            }

            if(nCol < 1) {
                mGuideStep.set(mHeading, mHeading, 0.0f);
            }

            // when we're really close ... i.e. when the bin width is really big ... we're done
            if (nCol > 1 && avgBinWidth > colString.length()*0.65f) {          // for now, when bin width > 1/2 FOV
                // require completion test to pass some min number of times in a row to believe it
                if (++mDoneCount >= minDoneCount) {
                    mDone = true;
                }
            }
            else
                mDoneCount = 0;         // reset the "done" counter

            mOpMode.telemetry.addData("data", "doneCount=%d", doneCount);

            // save column hits for next pass to help handle columns leaving the field of view of
            // the camera as we get close.
            mPrevColumns = columns;

        }

        mView.getHandler().post(new Runnable() {
            @Override
            public void run() {
                //synchronized (bmLock) {
                mView.setImageBitmap(mBmOut);
                mView.invalidate();
                //mView.setVisibility(View.VISIBLE);
                //}
            }
        });

        return mDone;  // are we done?
    }

    public void stop() {
    }
}

public class BigAuto extends OpMode {

    AutoLib.Sequence mSequence;             // the root of the sequence tree
    boolean bDone;                          // true when the programmed sequence is done
    boolean firstTime = true;
    DcMotor mMotors[];                      // motors, some of which can be null: assumed order is fr, br, fl, bl
    DcMotor motorLift;
    DcMotor motorOmni[];
    Servo servoGem[];
    ModernRoboticsI2cGyro mGyro1;           // gyro to use for heading information
    SensorLib.CorrectedMRGyro mCorrGyro;    // gyro corrector object
    ColorSensor mColorSensor;
    boolean bSetup, servoSetup = true;                         // true when we're in "setup mode" where joysticks tweak parameters
    SensorLib.PID mPid, mPid2;                     // PID controller for the sequence
    RS_Posterize mRsPosterize;              // RenderScript posterization filter

    VuforiaLib_FTC2017 mVLib;
    GoToCryptoBoxGuideStep binStep;

    AutoLib.Timer mTimer;

    // parameters of the PID controller for this sequence
    float Kp = 0.011f;        // motor power proportional term correction per degree of deviation
    float Ki = 0.005f;         // ... integrator term
    float Kd = 0;             // ... derivative term
    float KiCutoff = 20.0f;    // maximum angle error for which we update integrator

    public BigAuto() {
        msStuckDetectInit = 1000000;
        msStuckDetectInitLoop = 1000000;
        msStuckDetectLoop = 1000000;
        msStuckDetectStart = 1000000;
    }

    public void init() {}

    public void init(boolean bLookForBlue, boolean bGoStraight, boolean runCamera) {
        bSetup = false;      // start out in Kp/Ki setup mode
        AutoLib.HardwareFactory mf = null;
        final boolean debug = false;
        if (debug)
            mf = new AutoLib.TestHardwareFactory(this);
        else
            mf = new AutoLib.RealHardwareFactory(this);

        if(bGoStraight) {
            mTimer = new AutoLib.Timer(27.0);
        }
        else {
            mTimer = new AutoLib.Timer(25.0);
        }

        // get the motors: depending on the factory we created above, these may be
        // either dummy motors that just log data or real ones that drive the hardware
        // assumed order is fr, br, fl, bl

        mMotors = new DcMotor[4];
        mMotors[0] = mf.getDcMotor("fr");
        mMotors[1] = mf.getDcMotor("br");
        mMotors[2] = mf.getDcMotor("fl");
        mMotors[3] = mf.getDcMotor("bl");

        motorOmni = new DcMotor[2];
        motorOmni[0] = hardwareMap.dcMotor.get("o1");
        motorOmni[1] = hardwareMap.dcMotor.get("o2");

        servoGem = new Servo[2];
        servoGem[0] = mf.getServo("gem1");
        servoGem[1] = mf.getServo("gem2");
        motorLift = mf.getDcMotor("lift");

        mMotors[2].setDirection(DcMotor.Direction.REVERSE);
        mMotors[3].setDirection(DcMotor.Direction.REVERSE);

        mMotors[0].setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mMotors[1].setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mMotors[2].setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        mMotors[3].setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        mColorSensor = mf.getColorSensor("color");

        motorLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorOmni[0].setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorOmni[1].setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        // get hardware gyro(s) -- we support either one or two, where the second is assumed
        // to be mounted in opposite z-orientation to the first.
        mGyro1 = (ModernRoboticsI2cGyro) mf.getGyro("gyro");

        // wrap gyro(s) in an object that calibrates them and corrects their output
        mCorrGyro = new SensorLib.CorrectedMRGyro(mGyro1);
        mCorrGyro.calibrate();

        // create a PID controller for the sequence
        mPid = new SensorLib.PID(Kp, Ki, Kd, KiCutoff);    // make the object that implements PID control algorithm
        mPid2 = new SensorLib.PID(0.01f, 0, 0, KiCutoff);

        // create an autonomous sequence with the steps to drive
        // several legs of a polygonal course ---
        float power = 0.5f;

        // create the root Sequence for this autonomous OpMode
        mSequence = new AutoLib.LinearSequence();

        boolean bUseEncoders = false;
        if (bUseEncoders) {
            // add a bunch of encoder-counted "legs" to the sequence - use Gyro heading convention of positive degrees CW from initial heading
            // wrap motors to simplify use of encoders

            //DELETED
        }
        else {
            // add a bunch of timed "legs" to the sequence - use Gyro heading convention of positive degrees CW from initial heading
            float leg = 2;  // time along each leg of the polygon

            boolean test = true;
            if(runCamera) {
                mVLib = new VuforiaLib_FTC2017(); // init vuforia
                mVLib.init(this, null);
                binStep = new GoToCryptoBoxGuideStep(this, mVLib, bLookForBlue ? "^b+" : "^r+", 0.2f, 0, mCorrGyro, hardwareMap, bLookForBlue, true); //step for cryptobox navigation
                mSequence.add(new VuforiaGetMarkStep(this, mVLib, binStep)); //get mark
                mSequence.add(new AutoLib.GuidedTerminatedDriveStep(this, binStep, null, mMotors)); //activate cryptobox step
            }
            else if(bGoStraight) { // full code
                mSequence.add(new AutoLib.ServoSetPosition(servoGem[1], 0.0)); // move the gem arm down
                mSequence.add(new AutoLib.LogTimeStep(this, "wait", 0.25));
                mSequence.add(new AutoLib.ServoSetPosition(servoGem[0], 0.50));
                mSequence.add(new AutoLib.LogTimeStep(this, "wait", 0.75));

                mVLib = new VuforiaLib_FTC2017(); // init vuforia
                mVLib.init(this, null);

                mSequence.add(new AutoLib.TimedMotorStep(motorLift, 1.0, 0.5, true)); // move lifter up

                mSequence.add(new AutoLib.JewelStep(this, bLookForBlue ? 0 : 1, mColorSensor, mMotors, power, mCorrGyro, mPid, servoGem[0]));

                mSequence.add(new AutoLib.ServoSetPosition(servoGem[1], 0.6467503)); // move the gem arm up
                mSequence.add(new AutoLib.ServoSetPosition(servoGem[0], 0.0));
                mSequence.add(new AutoLib.LogTimeStep(this, "wait", 1.0));

                mSequence.add(new AutoLib.GyroRotateStep(this, mMotors, 0.4f, mCorrGyro, mPid, 90, 10));

                mSequence.add(new AutoLib.SquirrelyGyroTimedDriveStep(this, 180, 90, mCorrGyro, mPid, mMotors, 0.15f, 0.6f, true));
                mSequence.add(new AutoLib.SquirrelyGyroTimedDriveStep(this, 90, 90, mCorrGyro, mPid, mMotors, 0.1f, 0.5f, true));

                binStep = new GoToCryptoBoxGuideStep(this, mVLib, bLookForBlue ? "^b+" : "^r+", 0.3f, bLookForBlue ? 0 : 180, mCorrGyro, hardwareMap, bLookForBlue, true); //step for cryptobox navigation
                mSequence.add(new AutoLib.LogTimeStep(this, "wait", 1.0f));

                mSequence.add(new AutoLib.SquirrelyGyroTimedDriveStep(this, -90, 90, mCorrGyro, mPid, mMotors, 1.0f, 0.6f, true));
                mSequence.add(new VuforiaGetMarkStep(this, mVLib, binStep)); //get mark

                mSequence.add(new AutoLib.GyroRotateStep(this, mMotors, 0.4f, mCorrGyro, mPid, bLookForBlue ? 0 : 180, 6));

                /*if(!bLookForBlue) {
                    mSequence.add(new AutoLib.SquirrelyGyroTimedDriveStep(this, 180, 180, mCorrGyro, mPid, mMotors, 0.5f, 1.0f, true));
                    mSequence.add(new AutoLib.SquirrelyGyroTimedDriveStep(this, -90, 180, mCorrGyro, mPid, mMotors, 0.5f, 0.6f, true));
                }*/

                mSequence.add(new AutoLib.TimedMotorStep(motorLift, 1.0, 0.8, true)); // move lifter up

                mSequence.add(new AutoLib.GuidedTerminatedDriveStep(this, binStep, null, mMotors)); //activate cryptobox step

                mSequence.add(new AutoLib.SquirrelyGyroTimedDriveStep(this, bLookForBlue ? 180 : 0, bLookForBlue ? 0 : 180, mCorrGyro, mPid, mMotors, 0.30f, 0.4f, true));
                mSequence.add(new AutoLib.TimedMotorStep(motorLift, -1.0, 1.0, true)); // move lifter down

                mSequence.add(new AutoLib.MotorSetPower(motorOmni[0], -0.6f));
                mSequence.add(new AutoLib.MotorSetPower(motorOmni[1], 0.6f));

                mSequence.add(new AutoLib.LogTimeStep(this, "wait", 0.70));

                mSequence.add(new AutoLib.SquirrelyGyroTimedDriveStep(this, bLookForBlue ? 180 : 0, bLookForBlue ? 0 : 180, mCorrGyro, mPid, mMotors, 0.20f, 0.4f, true));

                mSequence.add(new AutoLib.MotorSetPower(motorOmni[0], 0.0f));
                mSequence.add(new AutoLib.MotorSetPower(motorOmni[1], 0.0f));
            }
            else {
                mSequence.add(new AutoLib.ServoSetPosition(servoGem[1], 0.0)); // move the gem arm down
                mSequence.add(new AutoLib.LogTimeStep(this, "wait", 0.75));
                mSequence.add(new AutoLib.ServoSetPosition(servoGem[0], 0.95));
                mSequence.add(new AutoLib.LogTimeStep(this, "wait", 0.75));

                mVLib = new VuforiaLib_FTC2017(); // init vuforia
                mVLib.init(this, null);

                mSequence.add(new AutoLib.TimedMotorStep(motorLift, 1.0, 0.5, true)); // move lifter up

                mSequence.add(new AutoLib.JewelStep(this, bLookForBlue ? 0 : 1, mColorSensor, mMotors, power, mCorrGyro, mPid, servoGem[0]));

                mSequence.add(new AutoLib.ServoSetPosition(servoGem[1], 0.6467503)); // move the gem arm up
                mSequence.add(new AutoLib.ServoSetPosition(servoGem[0], 0.0));
                mSequence.add(new AutoLib.LogTimeStep(this, "wait", 1.0));

                mSequence.add(new AutoLib.GyroRotateStep(this, mMotors, 0.4f, mCorrGyro, mPid, 90, 6));

                mSequence.add(new AutoLib.SquirrelyGyroTimedDriveStep(this, bLookForBlue ? 0 : 180, 90, mCorrGyro, mPid, mMotors, 0.5f, bLookForBlue ? 3.0f : 2.5f, true));
                mSequence.add(new AutoLib.SquirrelyGyroTimedDriveStep(this, 270, 90, mCorrGyro, mPid, mMotors, 0.5f, 0.3f, true));

                binStep = new GoToCryptoBoxGuideStep(this, mVLib, bLookForBlue ? "^b+" : "^r+", 0.3f, 90, mCorrGyro, hardwareMap, bLookForBlue, false); //step for cryptobox navigation
                mSequence.add(new VuforiaGetMarkStep(this, mVLib, binStep)); //get mark

                mSequence.add(new AutoLib.TimedMotorStep(motorLift, 1.0, 0.8, true)); // move lifter up

                mSequence.add(new AutoLib.GuidedTerminatedDriveStep(this, binStep, null, mMotors)); //activate cryptobox step

                mSequence.add(new AutoLib.SquirrelyGyroTimedDriveStep(this, 270, 90, mCorrGyro, mPid, mMotors, 0.30f, 0.4f, true));

                mSequence.add(new AutoLib.TimedMotorStep(motorLift, -1.0, 1.0, true)); // move lifter down

                mSequence.add(new AutoLib.MotorSetPower(motorOmni[0], -0.5f));
                mSequence.add(new AutoLib.MotorSetPower(motorOmni[1], 0.5f));

                mSequence.add(new AutoLib.LogTimeStep(this, "wait", 0.70));

                mSequence.add(new AutoLib.SquirrelyGyroTimedDriveStep(this, 270, 90, mCorrGyro, mPid, mMotors, 0.20f, 0.4f, true));

                mSequence.add(new AutoLib.MotorSetPower(motorOmni[0], 0.0f));
                mSequence.add(new AutoLib.MotorSetPower(motorOmni[1], 0.0f));

            }
        }

        // start out not-done
        bDone = false;
    }

    @Override
    public void loop() {
        if(firstTime) {
            firstTime = false;
            mTimer.start();
        }
        if(mTimer.done()) {
            binStep.finish();
        }
        else
            telemetry.addData("time", mTimer.remaining());

        if (gamepad1.y)
            bSetup = true;      // enter "setup mode" using controller inputs to set Kp and Ki
        if (gamepad1.x)
            bSetup = false;     // exit "setup mode"

        if (bSetup) {           // "setup mode"
            // adjust PID parameters by joystick inputs
            Kp -= (gamepad1.left_stick_y * 0.0001f);
            Ki -= (gamepad1.right_stick_y * 0.0001f);
            // update the parameters of the PID used by all Steps in this test
            mPid.setK(Kp, Ki, Kd, KiCutoff);
            // log updated values to the operator's console
            telemetry.addData("Kp = ", Kp);
            telemetry.addData("Ki = ", Ki);
            return;
        }

        // until we're done, keep looping through the current Step(s)
        if (!bDone) {
            bDone = mSequence.loop();       // returns true when we're done
        }
        else {
            telemetry.addData("sequence finished", "");
        }

        telemetry.addData("heading", mCorrGyro.getHeading());
    }

    @Override public void start()
    {
        // start out not-done
        bDone = false;

        // start Vuforia scanning
        mVLib.start();

        // create a RenderScript posterizer -- we do it here because it takes too long to do in init()
        mRsPosterize = new RS_Posterize();
        mRsPosterize.createScript(this.hardwareMap.appContext);

        // tell the camera-based guide step about the RenderScript posterizer it should use
        binStep.setPosterizer(mRsPosterize);
    }

    @Override
    public void stop() {
        super.stop();
        mCorrGyro.stop();        // release the physical sensor(s) we've been using for azimuth data
        mVLib.stop();
    }
}