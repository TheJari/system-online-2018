/* Copyright (c) 2014 Qualcomm Technologies Inc

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted (subject to the limitations in the disclaimer below) provided that
the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

Neither the name of Qualcomm Technologies Inc nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

/**
 * TeleOp Mode
 * <p>
 * Enables control of the robot via the gamepad using Squirrely Wheels
 * Like normal tank drive (left and right sticks in y direction) +
 * squirrley traversal (left and right sticks in x direction)
 */
@TeleOp(name="USE THIS ONE", group="TeleOp")  // @Autonomous(...) is the other common choice
public class TeleOp2018 extends OpMode {

	DcMotor motorFrontRight;
	DcMotor motorFrontLeft;
	DcMotor motorBackRight;
	DcMotor motorBackLeft;
	DcMotor motorLift;
    DcMotor motorOmni[];
	Servo servoGrabberRight[];
	Servo servoGrabberLeft[];
	Servo servoGemArm[];

	boolean mGrabberOpen = false;
	boolean mGrabButton = false;
	boolean xPressed = false;
	boolean mMeccanumGrab = true;



	float power = 0;

	float gem1 = 0;
	float gem2 = 0;

	/**
	 * Constructor
	 */
	public TeleOp2018() {

	}

	/*
	 * Code to run when the op mode is first enabled goes here
	 *
	 * @see com.qualcomm.robotcore.eventloop.opmode.OpMode#start()
	 */
	public void init() {

		/*
		 * Use the hardwareMap to get the dc motors and servos by name. Note
		 * that the names of the devices must match the names used when you
		 * configured your robot and created the configuration file.
		 */

		/*
		 * For this test, we assume the following,
		 *   There are four motors
		 *   "fl" and "bl" are front and back left wheels
		 *   "fr" and "br" are front and back right wheels
		 */

		servoGrabberRight = new Servo[2];
		servoGrabberLeft = new Servo[2];
		servoGemArm = new Servo[2];
        motorOmni = new DcMotor[2];

		motorFrontRight = hardwareMap.dcMotor.get("fr");
		motorFrontLeft = hardwareMap.dcMotor.get("fl");
		motorBackRight = hardwareMap.dcMotor.get("br");
		motorBackLeft = hardwareMap.dcMotor.get("bl");
		motorLift = hardwareMap.dcMotor.get("lift");
        motorOmni[0] = hardwareMap.dcMotor.get("o1");
        motorOmni[1] = hardwareMap.dcMotor.get("o2");
		servoGemArm[0] = hardwareMap.servo.get("gem1");
		servoGemArm[1] = hardwareMap.servo.get("gem2");

		if(!mMeccanumGrab) {
			servoGrabberLeft[0] = hardwareMap.servo.get("l1");
			servoGrabberLeft[1] = hardwareMap.servo.get("l2");
			servoGrabberRight[0] = hardwareMap.servo.get("r1");
			servoGrabberRight[1] = hardwareMap.servo.get("r2");
		}

		motorFrontRight.setDirection(DcMotor.Direction.REVERSE);
		motorBackRight.setDirection(DcMotor.Direction.REVERSE);

		motorBackLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
		motorBackRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
		motorFrontLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
		motorFrontRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

		motorLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
		motorOmni[0].setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
		motorOmni[1].setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
	}


	/*
	 * This method will be called repeatedly in a loop
	 *
	 * @see com.qualcomm.robotcore.eventloop.opmode.OpMode#run()
	 */
	public void loop() {
		double fr, br, fl, bl;

		// tank drive
		// note that if y equal -1 then joystick is pushed all of the way forward.
		float left = scaleInput(gamepad1.left_stick_y);
		float right = scaleInput(gamepad1.right_stick_y);

		// squirrely drive is controlled by the x axis of both sticks
		float xLeft = -scaleInput(gamepad1.left_stick_x);
		float xRight = -scaleInput(gamepad1.right_stick_x);

		fr = Range.clip(right+xRight, -1, 1);
		br = Range.clip(right-xRight, -1, 1);
		fl = Range.clip(left+xLeft, -1, 1);
		bl = Range.clip(left-xLeft, -1, 1);

		// write the values to the motors
		if(gamepad1.left_bumper) {
			motorFrontRight.setPower(fr/2);
			motorBackRight.setPower(br/2);
			motorFrontLeft.setPower(fl/2);
			motorBackLeft.setPower(bl/2);
		}
		else {
			motorFrontRight.setPower(fr);
			motorBackRight.setPower(br);
			motorFrontLeft.setPower(fl);
			motorBackLeft.setPower(bl);
		}

		if(gamepad2.dpad_down) {
			motorLift.setPower(-1.0f);
		}
		if(gamepad2.dpad_up) {
			motorLift.setPower(1.0f);
		}
		if(!gamepad2.dpad_down && !gamepad2.dpad_up) {
			motorLift.setPower(0.0f);
		}

		gem1 += -gamepad2.left_stick_y / 1000;
		gem2 += -gamepad2.right_stick_y / 1000;
		if(gem1 > 1) {
			gem1 = 1;
		}
		if(gem1 < 0) {
			gem1 = 0;
		}
		if(gem2 > 1) {
			gem2 = 1;
		}
		if(gem2 < 0) {
			gem2 = 0;
		}
		servoGemArm[0].setPosition(0.0);
		servoGemArm[1].setPosition(0.5);

		if(!mMeccanumGrab) {
			if (gamepad2.x) {
				servoGrabberLeft[0].setPosition(0.4);
				servoGrabberLeft[1].setPosition(0.4);
				servoGrabberRight[0].setPosition(0.6);
				servoGrabberRight[1].setPosition(0.6);
			} else if (gamepad2.y) {
				servoGrabberLeft[0].setPosition(0.5);
				servoGrabberLeft[1].setPosition(0.5);
				servoGrabberRight[0].setPosition(0.5);
				servoGrabberRight[1].setPosition(0.5);
			} else if (gamepad2.a) {
				servoGrabberLeft[0].setPosition(0.6);
				servoGrabberLeft[1].setPosition(0.6);
				servoGrabberRight[0].setPosition(0.4);
				servoGrabberRight[1].setPosition(0.4);
			} else {
				servoGrabberLeft[0].setPosition(0.9 - 0.6 * gamepad2.left_trigger);
				servoGrabberLeft[1].setPosition(0.9 - 0.6 * gamepad2.left_trigger);
				servoGrabberRight[0].setPosition(0.1 + 0.6 * gamepad2.right_trigger);
				servoGrabberRight[1].setPosition(0.1 + 0.6 * gamepad2.right_trigger);
			}
		}
		else {
			float omniPower = 0.9f*(gamepad2.right_trigger-gamepad2.left_trigger+gamepad2.right_stick_y+gamepad2.left_stick_y);
			if(omniPower > 1) {
				omniPower = 1.0f;
			}
			if(omniPower < -1) {
				omniPower = -1.0f;
			}
			motorOmni[0].setPower(omniPower);
			motorOmni[1].setPower(-omniPower);
		}

		/*
		 * Send telemetry data back to driver station.
		 */
		telemetry.addData("Text", "*** v2.1 ***");
		telemetry.addData("front left/right power:", "%.2f %.2f", fl, fr);
		telemetry.addData("back left/right power:", "%.2f %.2f", bl, br);
		telemetry.addData("gem1:", gem1);
		telemetry.addData("gem2:", gem2);
		telemetry.addData("gamepad1:", gamepad1);
		telemetry.addData("gamepad2", gamepad2);
	}

	/*
	 * Code to run when the op mode is first disabled goes here
	 * 
	 * @see com.qualcomm.robotcore.eventloop.opmode.OpMode#stop()
	 */
	public void stop() {

	}

	/*
	 * This method scales the joystick input so for low joystick values, the 
	 * scaled value is less than linear.  This is to make it easier to drive
	 * the robot more precisely at slower speeds.
	 */
	float scaleInput(float dVal)  {
		return dVal*dVal*dVal;		// maps {-1,1} -> {-1,1}
	}

}
