/* Copyright (c) 2014 Qualcomm Technologies Inc

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted (subject to the limitations in the disclaimer below) provided that
the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

Neither the name of Qualcomm Technologies Inc nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

import static java.lang.Math.sqrt;

/**
 * TeleOp Mode
 * <p>
 * Enables control of the robot via the gamepad using Squirrely Wheels
 * Like normal tank drive (left and right sticks in y direction) +
 * squirrley traversal (left and right sticks in x direction)
 */
@TeleOp(name="TestDrive10565", group="TeleOp")  // @Autonomous(...) is the other common choice
public class TestDrive10565 extends OpMode {

	DcMotor motorFrontRight;
	DcMotor motorFrontLeft;
	DcMotor motorBackRight;
	DcMotor motorBackLeft;
	DcMotor motorLift;
	Servo servoGemArm;
	Servo servoGrabberRight[];
	Servo servoGrabberLeft[];

	boolean mGrabberOpen = false;
	boolean mGrabButton = false;
	boolean mGrabberTest = true;
	boolean xPressed = false;

	float power = 0;

	/**
	 * Constructor
	 */
	public TestDrive10565() {

	}

	/*
	 * Code to run when the op mode is first enabled goes here
	 *
	 * @see com.qualcomm.robotcore.eventloop.opmode.OpMode#start()
	 */
	public void init() {

		/*
		 * Use the hardwareMap to get the dc motors and servos by name. Note
		 * that the names of the devices must match the names used when you
		 * configured your robot and created the configuration file.
		 */

		/*
		 * For this test, we assume the following,
		 *   There are four motors
		 *   "fl" and "bl" are front and back left wheels
		 *   "fr" and "br" are front and back right wheels
		 */

		servoGrabberRight = new Servo[2];
		servoGrabberLeft = new Servo[2];

		motorFrontRight = hardwareMap.dcMotor.get("fr");
		motorFrontLeft = hardwareMap.dcMotor.get("fl");
		motorBackRight = hardwareMap.dcMotor.get("br");
		motorBackLeft = hardwareMap.dcMotor.get("bl");
		motorLift = hardwareMap.dcMotor.get("lift");
		servoGemArm = hardwareMap.servo.get("gem");
		servoGrabberLeft[0] = hardwareMap.servo.get("l1");
		servoGrabberLeft[1] = hardwareMap.servo.get("l2");
		servoGrabberRight[0] = hardwareMap.servo.get("r1");
		servoGrabberRight[1] = hardwareMap.servo.get("r2");
		motorFrontRight.setDirection(DcMotor.Direction.REVERSE);
		motorBackRight.setDirection(DcMotor.Direction.REVERSE);
	}


	/*
	 * This method will be called repeatedly in a loop
	 *
	 * @see com.qualcomm.robotcore.eventloop.opmode.OpMode#run()
	 */
	public void loop() {
		// tank drive
		// note that if y equal -1 then joystick is pushed all of the way forward.
		float left = gamepad1.left_stick_y;
		float right = gamepad1.right_stick_y;

		// clip the right/left values so that the values never exceed +/- 1
		left = Range.clip(left, -1, 1);
		right = Range.clip(right, -1, 1);

		// scale the joystick value to make it easier to control
		// the robot more precisely at slower speeds.
		left =  (float)scaleInput(left);
		right = (float)scaleInput(right);

		// squirrely drive is controlled by the x axis of both sticks
		float xLeft = -gamepad1.left_stick_x;
		float xRight = -gamepad1.right_stick_x;

		// combine turning and squirrely drive inputs and
		double fr = Range.clip(right+xRight, -1, 1);
		double br = Range.clip(right-xRight, -1, 1);
		double fl = Range.clip(left+xLeft, -1, 1);
		double bl = Range.clip(left-xLeft, -1, 1);

		// write the values to the motors
		if(gamepad1.left_bumper) {
			motorFrontRight.setPower(fr);
			motorBackRight.setPower(br);
			motorFrontLeft.setPower(fl);
			motorBackLeft.setPower(bl);
		}
		else {
			motorFrontRight.setPower(fr/2);
			motorBackRight.setPower(br/2);
			motorFrontLeft.setPower(fl/2);
			motorBackLeft.setPower(bl/2);
		}

		if(gamepad1.dpad_down) {
			motorLift.setPower(-1.0f);
		}
		if(gamepad1.dpad_up) {
			motorLift.setPower(1.0f);
		}
		if(!gamepad1.dpad_down && !gamepad1.dpad_up) {
			motorLift.setPower(0.0f);
		}

		if(gamepad1.b) {
			servoGemArm.setPosition(0.95f);
		}
		else if(gamepad1.a) {
			servoGemArm.setPosition(0.0f);
		}

		if(gamepad1.x && !xPressed) {
			if(mGrabberTest) {
				mGrabberTest = false;
			}
			else {
				mGrabberTest = true;
			}
			xPressed = true;
		}
		else if(!gamepad1.x) {
			xPressed = false;
		}

		if(mGrabberTest){
			servoGrabberLeft[0].setPosition(1-gamepad1.right_trigger);
			servoGrabberLeft[1].setPosition(1-gamepad1.right_trigger);
			servoGrabberRight[0].setPosition(gamepad1.right_trigger);
			servoGrabberRight[1].setPosition(gamepad1.right_trigger);
		}
		else {
			if(gamepad1.right_bumper && !mGrabButton){
				if(mGrabberOpen) {
					servoGrabberLeft[0].setPosition(0);
					servoGrabberLeft[1].setPosition(0);
					servoGrabberRight[0].setPosition(1);
					servoGrabberRight[1].setPosition(1);
					mGrabberOpen = false;
				}
				else {
					servoGrabberLeft[0].setPosition(1);
					servoGrabberLeft[1].setPosition(1);
					servoGrabberRight[0].setPosition(0);
					servoGrabberRight[1].setPosition(0);
					mGrabberOpen = true;
				}
				mGrabButton = true;
			}
			else if(!gamepad1.right_bumper){
				mGrabButton = false;
			}
		}

		/*
		 * Send telemetry data back to driver station.
		 */
		telemetry.addData("Text", "*** v2.1 ***");
		telemetry.addData("front left/right power:", "%.2f %.2f", fl, fr);
		telemetry.addData("back left/right power:", "%.2f %.2f", bl, br);
		telemetry.addData("gamepad1:", gamepad1);
		//telemetry.addData("gamepad2", gamepad2);
	}

	/*
	 * Code to run when the op mode is first disabled goes here
	 * 
	 * @see com.qualcomm.robotcore.eventloop.opmode.OpMode#stop()
	 */
	public void stop() {

	}

	/*
	 * This method scales the joystick input so for low joystick values, the 
	 * scaled value is less than linear.  This is to make it easier to drive
	 * the robot more precisely at slower speeds.
	 */
	double scaleInput(double dVal)  {
		return dVal*dVal*dVal;		// maps {-1,1} -> {-1,1}
	}

}
